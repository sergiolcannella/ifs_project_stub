<?php
	require_once __DIR__ . '/public/authorize.php';
	use Infusionsoft\Infusionsoft;
	
	$req = $_REQUEST["rawRequest"];
	$obj = json_decode($req, true);
	
	//CONTACT
	$firstname = $obj["q3_name"]["first"];
	$lastname = $obj["q3_name"]["last"];
	$email = $obj["q4_email"];
	$address1 = $obj["q5_address"]["addr_line1"];
	$address2 = $obj["q5_address"]["addr_line2"];
	$city = $obj["q5_address"]["city"];
	$state = $obj["q5_address"]["state"];
	$zip = $obj["q5_address"]["postal"];
	$phone = $obj["q6_phoneNumber"]["full"];
	$favoritecolor = $obj["q9_favoriteColor"];
	$reasonfor = $obj["q8_reasonFor"];
	$companyname = $obj["q11_companyName"];
	$industry = $obj["q13_industry"];
	$certificate = $obj["q15_certificateNumber"];
	$community = $obj["q16_community"];

	//COMPANY

	$businesstype = $obj["q17_businessType"];
	$businesstypeother = $obj["q17_businessType"]["other"];
	$company = [];
	
	if($businesstypeother <> "S" && $businesstypeother <> "I" && $businesstypeother <> "L")
	{
		$company = ["_BusinessTypeOther" => $businesstypeother];
		$businesstype = "Other";
	}

	$company = $company + ["Company" => $companyname,
	"_Industry" => $industry,
	"_CertificateNumber" => $certificate,
	"_BusinessType" => $businesstype,
	"_Community" => $community];

	$contact = ["FirstName" => $firstname,
	"LastName" => $lastname,
	"Email" => $email,
	"StreetAddress1" => $address1,
	"StreetAddress2" => $address2,
	"City" => $city,
	"PostalCode" => $zip,
	"State" => $state,
	"Phone1" => $phone,
	"_FavoriteColor" => $favoritecolor,
	"_ReasonforRequest" => $reasonfor,
	"Company" => $companyname];

	$ContactID = $infusionsoft->contacts("xml")->addWithDupCheck($contact, "Email");


	//QUERY COMPANY FOR UPDATE
	$results = $infusionsoft->companies("xml")->where("company_name",$companyname)->get();
	
	//UPDATE OR ADD COMPANY and JOIN on CompanyID
	if(count($results) == 0)
	{
		$CompanyID = $infusionsoft->data()->add("Company", $company);
		
		$data = ["CompanyID" => $CompanyID];
		$infusionsoft->contacts("xml")->update($ContactID , $data);

	} else {
		$obj = json_decode($results[0]);
		$infusionsoft->data()->update("Company", $obj->{'id'}, $company);
		
		$data = array("CompanyID" => $obj->{"id"});
		$infusionsoft->contacts("xml")->update($ContactID , $data);
	}
?> 
